package ru.mla;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс для последовательного поиска числа в массиве случайных значений
 *
 * @author Mironova L.
 */
public class IncrementalSearch {

    public static final int MIN_VALUE = -25;
    public static final int VALUES_RANGE = 51;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = inputArraySize();

        System.out.print("Введите число для поиска: ");
        int number = scanner.nextInt();

        int[] array = randomArrayFilling(size);
        System.out.println(Arrays.toString(array));

        int index = searchNumber(number, array);
        if (index == -1) {
            System.out.println("нет такого значения");
        } else {
            System.out.println("индекс элемента в последовательности " + index);
        }
    }

    /**
     * Заполняет массив случайными значениями в заданном интервале от MIN_VALUE в диапазоне VALUES_RANGE
     *
     * @return массив случайных чисел
     */
    private static int[] randomArrayFilling(int size) {
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = MIN_VALUE + (int) (Math.random() * VALUES_RANGE);
        }
        return array;
    }

    /**
     * Ищет в массиве случайных значений введенное с консоли число
     *
     * @param number введенное с консоли число
     * @param array  массив случайных чисел
     * @return -1, если такого числа нет в массиве, или индекс введенного числа в массиве, если это число есть в массиве
     */
    private static int searchNumber(int number, int[] array) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * Сохраняет введенный с консоли размер массива
     *
     * @return введенный с консоли размер массива
     */
    private static int inputArraySize() {
        Scanner scanner = new Scanner(System.in);
        int size;
        do {
            System.out.print("Введите размер массива: ");
            size = scanner.nextInt();
            if (size <= 1) {
                System.out.println("Некорректные данные");
            }
        } while (size <= 1);
        return size;
    }
}